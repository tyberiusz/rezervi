# lib/tasks/default.rake
if %w[development test].include? Rails.env
  require 'rubocop/rake_task'
  RuboCop::RakeTask.new

  task(:default).clear.enhance(%w[db:test:prepare rubocop spec immigrant:check_keys brakeman:run])
end
