// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery2
//= require turbolinks
//= require rails-ujs
//= require materialize
//= require_tree .
$(document).on('turbolinks:load', function() {

 //$('input').updateTextFields();
 //$('select').material_select();
  $('select').formSelect();
  $('.dropdown-trigger').dropdown();
  $('.sidenav').sidenav();
  $('.modal').modal();

 $('.tooltipped').tooltip({delay: 50});


  $('.collapsible').collapsible();

    $('select').formSelect();
    $('.tabs').tabs();
 $('.pushpin').pushpin();
 // start carrousel
   $('.carousel.carousel-slider').carousel({
      fullWidth: true,
      indicators: true
   });


   // move next carousel
   $('.moveNextCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('next');
   });

   // move prev carousel
   $('.movePrevCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('prev');
   });

   //panel

   var panel = $(".panel-expendable");
var switcher = $(".extra-content-switcher");
//var content = $(".content");
//var open = $(".extra-content-open");

// handle click and add class

  
   switcher.click(function(e) {
  
   //e.preventDefault();
   //e.stopPropagation();  
   var label = $(this).find(".switcher_text").text().trim();

   if(label == "zwiń") {
     $(this).find(".switcher_text").text("rozwiń");
     $(this).find(".switcher_icon").text("expand_more");
     //$(".toggle_content").hide(300);
     $(this).parents('.panel-expendable').find('.extra-content').hide();
     $(this).parents('.panel-expendable').removeClass("active-panel");
   }
   else {     
      $(this).find(".switcher_text").text("zwiń");
      $(this).find(".switcher_icon").text("expand_less");
      $(this).parents('.panel-expendable').find('.extra-content').show();
      $(this).parents('.panel-expendable').addClass("active-panel");
   }
   return false; // prevent default action of the click
    
  });

   panel.on("click", function(){

    //e.preventDefault();
    //e.stopPropagation();
    var label = $(this).find(".switcher_text").text().trim();
    if(label == "zwiń") {
     //$(this).find(".switcher_text").text("rozwiń");
     //$(this).find(".switcher_icon").text("expand_more");
     //$(".toggle_content").hide(300);
     //$(this).find('.extra-content').hide();
   }
   else {     
      $(this).find(".switcher_text").text("zwiń");
      $(this).find(".switcher_icon").text("expand_less");
      $(this).find('.extra-content').show();
      $(this).addClass("active-panel");
   }
   //return false; // prevent default action of the click
    //$(this).find('.btn').on("click", function(e){
      //if (e.target !== this)
      //  return;
    //});
    //$(this).find('.content').show();
    //console.log(panel);
    //console.log($(this).class)
//$('.tabs').updateTabIndicator();
        //$('.tabs').tabs('updateTabIndicator');



    //console.log($(this));
    //$(this).find('.content').removeClass('hide-on-small-only');
    
    $(this).find('.extra-content').show();
    $('.tabs').tabs('updateTabIndicator');
    //$(this).addClass('active-panel');

    


    $('.open_modal').click(function(e) {
      $('#overlay').fadeIn("slow");
      //var x = $("body")[0].scrollHeight;
      $("body").css("overflow", "hidden");
      //alert(x);
      //$('#overlay').css('top', x - 300)
    });

    $('.close_modal').click(function(e) {
      $("body").css("overflow", "auto");
      $('#overlay').fadeOut("slow");
      
    });

   // var open_modal = function() {
   //   $('#overlay').fadeIn("slow");
   // }
    
    
    //close.on("click", function(){
    //   //$(this).parents('.panel').find('.content').hide();
    //   $(this).parents('.panel-expendable').find('.content').addClass('hide-on-small-only');
    //   $(this).parents('.panel-expendable').find('.extra-content').hide();
    //   $(this).parents('.panel-expendable').removeClass('active-panel');
    //   $(this).parents('.panel-expendable').find('.extra-content-close').hide();
    //   $(this).parents('.panel-expendable').find('.extra-content-open').show();
    //  return false; // prevent default action of the click
    //})
  })
})




