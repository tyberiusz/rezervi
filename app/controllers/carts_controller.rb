class CartsController < ApplicationController
  include CurrentCart
  rescue_from ActiveRecord::RecordNotFound, with: :invalid_cart
  #before_action :set_cart, only: [:cart, :show, :edit, :update, :destroy]
  #before_action :current_cart, only: [:show]

  # GET /carts
  # GET /carts.json
  def index
    @carts = Cart.all
  end

  # GET /carts/1
  # GET /carts/1.json
  def show
    #set_cart
    #current_cart
    @cart = current_cart
    @cart_bookings = @cart.cart_bookings
  end

  def resume
    #set_cart
    #current_cart
    @cart = current_cart
    @cart_bookings = @cart.cart_bookings
  end

  def confirm
    @cart = current_cart
    @cart_bookings = @cart.cart_bookings

    @order = Order.create(kind_of_booking: @cart.kind_of_booking,
                           price_with_ducat: @cart.cart_total,
                           heart_ducat_price: @cart.cart_discount_heart_count,
                           price: (@cart.cart_total - @cart.cart_discount_heart_count),
                           total_save: @cart.cart_discount_standard_price
)
    @cart_bookings.each do |cart_booking|
      @order_booking = OrderBooking.create(order_id: @order.id, 
                            company_id: cart_booking.company_id, 
                            date_of: cart_booking.date_of, 
                            hash_of_uniq: cart_booking.hash_of_uniq )
      cart_booking.cart_items.each do |cart_item|
        @order_item = OrderItem.create(

        order_id: @order.id,
        service_id: cart_item.service_id,
        employee_id: cart_item.employee_id,
        kind_of_booking: cart_item.kind_of_booking,
        current_step: cart_item.current_step,
        configured: cart_item.configured,
        starts_at: cart_item.starts_at,
        booking_order_id: @order_booking.id)

        
      end
     end

     @cart.destroy
  end

  def payment

  end

  def panel
    
  end

  # GET /carts/new
  def new
    @cart = Cart.new
  end

  # GET /carts/1/edit
  def edit
  end

  # POST /carts
  # POST /carts.json
  def create
    @cart = Cart.new(cart_params)
    puts params[:period] = 'sddc'
    respond_to do |format|
      if @cart.save
        format.html {  }
        format.json { render :show, status: :created, location: @cart }
      else
        format.html { render :new }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /carts/1
  # PATCH/PUT /carts/1.json
  def update
    respond_to do |format|
      if @cart.update(cart_params)
        format.html {}
        format.json { render :show, status: :ok, location: @cart }
      else
        format.html { render :edit }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /carts/1
  # DELETE /carts/1.json
  def destroy
    @cart.destroy if @cart.id == session[:cart_id]
    session[:cart_id] = nil
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Cart was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def set_kind
    @cart = current_cart
    @cart_bookings = @cart.cart_bookings
    if allowed_kind_of_booking.include?(params[:kind_of_booking])
      @cart.update_attributes(kind_of_booking: params[:kind_of_booking])
    end
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Cart was successfully set.' }
      format.js 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.

    def allowed_kind_of_booking
      ['booking', 'buying', 'prepayment']
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_params
      params.fetch(:cart, {})
    end  

    def invalid_cart
      logger.error "Attempt to access invalid cart #{params[:id]}"
      redirect_to root_path, notice: "That cart doesn't exist"
    end
end
