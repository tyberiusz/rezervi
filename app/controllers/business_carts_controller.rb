class BusinessCartsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :invalid_business_cart
  before_action :set_business_cart, only: [:cart, :show, :edit, :update, :destroy]

  # GET /business_carts
  # GET /business_carts.json
  def index
    @business_carts = BusinessCart.all
  end

  # GET /business_carts/1
  # GET /business_carts/1.json
  def show
  end

  # GET /business_carts/new
  def new
    @business_cart = BusinessCart.new
  end

  # GET /business_carts/1/edit
  def edit
  end

  # POST /business_carts
  # POST /business_carts.json
  def create
    @business_cart = BusinessCart.new(business_cart_params)
    puts params[:period] = 'sddc'
    respond_to do |format|
      if @business_cart.save
        format.html {  }
        format.json { render :show, status: :created, location: @business_cart }
      else
        format.html { render :new }
        format.json { render json: @business_cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /business_carts/1
  # PATCH/PUT /business_carts/1.json
  def update
    respond_to do |format|
      if @business_cart.update(business_cart_params)
        format.html {}
        format.json { render :show, status: :ok, location: @business_cart }
      else
        format.html { render :edit }
        format.json { render json: @business_cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /business_carts/1
  # DELETE /business_carts/1.json
  def destroy
    @business_cart.destroy if @business_cart.id == session[:business_cart_id]
    session[:business_cart_id] = nil
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'BusinessCart was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_business_cart
      @business_cart = BusinessCart.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def business_cart_params
      params.fetch(:business_cart, {})
    end

    def invalid_business_cart
      logger.error "Attempt to access invalid business_cart #{params[:id]}"
      redirect_to root_path, notice: "That business_cart doesn't exist"
    end
end
