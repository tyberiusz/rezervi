class CartItemsController < ApplicationController
	include CurrentCart
  before_action :set_cart_item, only: [:show, :edit, :update, :destroy, :configured]
  before_action :set_cart, only: [:new, :create]
  before_action :set_service, only: [:new, :create]
  # GET /_cart_items
  # GET /_cart_items.json
  def index
    @cart_items = CartItem.all
  end

  # GET /_cart_items/1
  # GET /_cart_items/1.json
  def show
   @date = params[:date] ? Date.parse(params[:date]) : Date.today
   @service = @cart_item.service
   @company = @service.company
   @employees = @service.company.employees
   @employee = Employee.find(@cart_item.employee_id)
    respond_to do |format|
      format.html
      #format.json { head :no_content }
      format.js
    end
  end

  # GET /_cart_items/new
  def new
    @cart_item = CartItem.new
  end

  # GET /_cart_items/1/edit
  def edit

  end

  # POST /_cart_items
  # POST /_cart_items.json
  def create
   	#@cart_item = CartItem.new
    
   	@cart_item = @service.cart_items.build(cart_item_params)
    set_current_cart
    @employees = @service.company.employees
    #raise params.inspect
       respond_to do |format|
      if @cart_item.save
      	#puts @cart_item.kind_of_booking
        format.html {  }
        format.json { render :show, status: :created, location: @cart_item }
        format.js
      else
        format.html { render :new }
        format.json { render json: @cart_item.errors, status: :unprocessable_entity }
        format.js
      end
    end
    puts '--C'
  end

  # PATCH/PUT /_cart_items/1
  # PATCH/PUT /_cart_items/1.json
  def update
       @service = @cart_item.service
   @company = @service.company
   @employees = @service.company.employees
  #if @cart_item.this_step > :select_employee
  @employee = Employee.find(@cart_item.employee_id) if @cart_item.employee_id.present?
  #end


    @date = params[:date] ? Date.parse(params[:date]) : Date.today
    puts 'date'
    puts @date
    if @cart_item.valid?
      if params[:cart_item][:back_button]     
        #puts 'VALID?'
        #puts @cart_item.valid?
        @cart_item[:current_step] = @cart_item.previous_step
        #params.delete :back_button
        #puts params

      else #@cart_item.valid?
       

         if @cart_item.this_step == @cart_item.avaliable_steps.last
          @cart_item[:configured] = true
         end
        
         @cart_item[:current_step] = @cart_item.next_step

      end
    #else
    puts "NOT VALID"  
    end
    ###
    ###

    if @cart_item.update(cart_item_params)
        puts @cart_item.configured

      if @cart_item.configured?
         cart_booking = CartBooking.find_or_create_by(cart_id: @cart_item.cart_id, company_id: @cart_item.service.company_id, date_of: @cart_item.starts_at)
         @cart_item.cart_booking_id = cart_booking.id
         @cart_item.save
         render 'configured'
      else

        respond_to do |format|
          format.html { redirect_to carts_path  }
          format.json { render :show, status: :ok, location: @cart_item }
          format.js
        end
      
      end
    else
        respond_to do |format|
          format.html { render :edit }
          format.json { render json: @cart_item.errors, status: :unprocessable_entity }
          format.js
      end
     
    end
  end

  # GET /_cart_items/1/configured
  def configured
    respond_to do |format|
    format.html { render 'configured' }
    #format.json { render :configured, status: :ok, location: @cart_item }
    format.js 
    end
  end

  # DELETE /_cart_items/1
  # DELETE /_cart_items/1.json
  def destroy
    @cart_item.destroy
    respond_to do |format|
      format.html
      #format.json { head :no_content }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart_item
      @cart_item = CartItem.find(params[:id])
    end

    def set_service
      @service = Service.find(params[:service_id])
    end

    def set_current_cart
      @cart_item.cart_id = current_cart.id
    end

    # Never trust parameters from the scary internet, only allow the whitelist through.
  def cart_item_params
    #params.require(:cart_item).permit(:kind_of_booking, :service_id, :cart_id)
    params.require(:cart_item).permit(:service_id, :kind_of_booking, :cart_id, :cart_booking_id, :employee_id, :current_step, :starts_at)
  end

end
