class CompaniesController < ApplicationController

	def create
		@company = Company.new(company_params)

		if @company.save
			redirect_to new_manager_registration_path
		end
	end

	def register_step_2
		@company = Company.find(params[:id])
	end

	private

	def company_params
		params.require(:company).permit(:plan_id, :time)
	end

end
