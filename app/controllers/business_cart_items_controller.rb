class BusinessCartItemsController < ApplicationController
include CurrentBusinessCart
  before_action :set_business_cart_item, only: [:show, :edit, :update, :destroy]
  before_action :set_business_cart, only: [:create]

  # GET /business_cart_items
  # GET /business_cart_items.json
  def index
    @business_cart_items = BusinessCartItem.all
  end

  # GET /business_cart_items/1
  # GET /business_cart_items/1.json
  def show
  end

  # GET /business_cart_items/new
  def new
    @business_cart_item = BusinessCartItem.new
  end

  # GET /business_cart_items/1/edit
  def edit
  end

  # POST /business_cart_items
  # POST /business_cart_items.json
def create
   puts 'C--'
    @plans = Plan.all
    @plan = Plan.find(params[:plan_id])
    @subscription = Subscription.find(params[:subscription_id])
    puts params
    puts params[:quantity] = 1
puts 'period'
    puts params[:period]
    @business_cart_item = @business_cart.add_product(@plan, params[:quantity], @subscription )
    puts params[:period]
    respond_to do |format|
      if @business_cart_item.save
        format.html {  }
        format.json { render :show, status: :created, location: @business_cart_item }
        format.js
      else
        format.html { render :new }
        format.json { render json: @business_cart_item.errors, status: :unprocessable_entity }
        format.js
      end
    end
    puts '--C'
  end

  # PATCH/PUT /business_cart_items/1
  # PATCH/PUT /business_cart_items/1.json
  def update
    respond_to do |format|
      if @business_cart_item.update(subscription_params)
        format.html { redirect_to business_carts_path  }
        format.json { render :show, status: :ok, location: @business_cart_item }
        format.js
      else
        format.html { render :edit }
        format.json { render json: @business_cart_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /business_cart_items/1
  # DELETE /business_cart_items/1.json
  def destroy
    @business_cart_item.destroy
    respond_to do |format|
      format.html
      #format.json { head :no_content }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_business_cart_item
      @business_cart_item = BusinessCartItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the whitelist through.
  def subscription_params
    params.require(:business_cart_item).permit(:subscription_id)
  end

  def business_cart_item_params
    params.require(:business_cart_item).permit(:plan_id, :business_cart_id, :quantity, :period, :subscription_id)
  end
end
