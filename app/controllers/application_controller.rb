class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include CurrentBusinessCart
  include CurrentCart

  before_action :set_business_cart
  before_action :set_cart
  after_action :set_cookie_info

  def set_cookie_info
  	cookies[:cookie_info] = true unless cookies[:cookie_info]
  end
end
