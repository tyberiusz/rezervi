class NgosController < ApplicationController

	http_basic_authenticate_with name: "filome", password: "rvi302", only: :raport

	def index
	end

	def raport
	end

	def new
		@ngo = Ngo.new
		@address = @ngo.build_address
	end


	def create
		@ngo = Ngo.new(allowed_params)
		@address = @ngo.build_address(allowed_params['address_attributes'])
		@ngo.ambassador_status = :candidate
		if @ngo.save
			redirect_to action: 'dziekujemy'
		else
			render 'new'
		end
	end

	def dziekujemy
	end

	def scrapper_city
		require 'nokogiri'
		require 'open-uri'

		url = "https://pl.wikipedia.org/wiki/Dane_statystyczne_o_miastach_w_Polsce"

		@page = Nokogiri::HTML(open(url))   
		#puts @page.class   # => Nokogiri::HTML::Document
		@table = @page.css("table.wikitable").search('tbody')

		 

        @rows = @table.css('tr').map do |row|
            city = City.new
            city.name = row.search('td')[0].css('a').text 
            city.district = row.search('td')[1].css('a').text.to_s.gsub("[a]", "") 
            city.vivodeship = row.search('td')[2].css('a').text.to_s
            city.amount = row.search('td')[4].text.to_i
            puts "#{city.name} - #{city.district} - #{city.vivodeship} - #{city.amount} "
            city.save!
        end


	end

	private

	def allowed_params
		params.require(:ngo).permit(
			:full_name,
			:email,
			:krs_number,
			:nip_number,
			:regon_number,
			:register_year,
			:opp,
			:business,
			:future_business_possible,
			:address,
			:correspondence_address,
			:city,
			:ambassador_status,
			address_attributes: 
								[
								:street_name,
								:street_number,
								:flat_number,
								:zip_code,
								:city_name,
								:city_id,
								:kind
								] 
			)
	end
end
