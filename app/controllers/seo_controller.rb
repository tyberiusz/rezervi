class SeoController < ApplicationController

  before_action :find_seo
  

  def show
 	# /:slug
 	#find_seo
  get_seoable_type
 	render set_template
 	redirect_301_from_old_adress
  end

  private

  def company
    puts "Company !!!!"
    #@seoable = Company.includes(:category, :services_groups).find(@seoable.id)
    @company = Company.eager_load(services_groups: [{ services: [:prepayment_discount] }], category: [:seo], city: [:seo] ).find(@seoable.id)
    puts 'end'
    @company
  end
  
  def find_seo
  	@seoable = Seo.includes(:seoable).friendly.find(params[:id]).seoable
    company if get_seoable_type == "Company"

  	rescue ActiveRecord::RecordNotFound
  	redirect_to root_url, :flash => { :error => "Record not found." } #, status: 404
  end

  def get_seoable_type
    @seoable.seo.seoable_type
  end
  
  def set_template
  	get_seoable_type.downcase.to_sym
  end
  
  def redirect_301_from_old_adress
  	if request.path != seo_link_path(@seoable.seo.friendly_id)
      redirect_to seo_link_path(@seoable.seo.friendly_id), status: 301
    end

  end

end
