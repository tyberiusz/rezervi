module SeoHelper

	def services_count(x)
	  last_number = x.to_s.last
	  last_number
	  if x == 1
	  	"#{x} usługa"
	  else
	  	if ["2", "3", "4"].include?(last_number) && ["12", "13", "14"].include?(x.to_s) == false
	  		"#{x} usługi" 
	  	else
	  		 "#{x} usług" 
	  	end
	  end
	end

	def humanize_time(secs)
  		[[60, "s"], [60, "min"], [24, "godz."], [Float::INFINITY, "dni"]].map{ |count, name|
    if secs > 0
      secs, n = secs.divmod(count)
      "#{n.to_i} #{name}" unless n.to_i==0
    end
  	}.compact.reverse.join(' ')
	end

  def humanize_duration(x)
    if x.is_a? Array
      min = humanize_time(x.first - Time.new(0))
      max = humanize_time(x.last - Time.new(0))
      result = "#{min} - #{max}"
    else
      result = humanize_time(x * 60 )
    end
    result
  end
end
