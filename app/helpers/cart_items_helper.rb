module CartItemsHelper

	def link_to_previous_step(f)
		puts 'BACK?'
		puts @cart_item.show_back_button?
		puts @cart_item.id
		puts @cart_item.avaliable_steps.first
		puts @cart_item.first_step?
		puts @cart_item.last_step?
		puts @cart_item.avaliable_steps.index(@cart_item.this_step)
		puts @cart_item.current_step
		puts @cart_item.current_step == @cart_item.avaliable_steps.index(@cart_item.this_step)
		if @cart_item.show_back_button?
		f.button "wstecz", name: "cart_item[back_button]", class: "waves-effect waves-light btn- btn-flat primary- secondary-text left-align-l" do
		#content_tag(:button, "wstecz"
			content_tag(:i, "arrow_back", class: "material-icons ") + content_tag(:span, "Wstecz")
			end
		end

	end


	def configured_employee(employee)
		name = employee.first
		job = employee.last
		content_tag(:span) do
			content_tag(:i, "person", class: 'material-icons icon-summary') +
			content_tag(:b, name) + ' ' +
			content_tag(:span, "#{ job}", class: "grey-text text-darken-2")
		end
	end

	def configured_date(date)
		#date = employee.first
		formatted_date = date_with_month_pl_dop(date)
		formatted_day = day_of_week_pl(date)
		content_tag(:span) do
			content_tag(:i, "event", class: 'material-icons icon-summary') +
			content_tag(:b, formatted_date) + ' ' +
			content_tag(:span, "(#{ formatted_day})", class: "grey-text text-darken-2")
		end
	end

	def configured_starts_at(date)
    	formatted_time = date.strftime("%H:%M")
		content_tag(:span) do
			content_tag(:i, "watch_later", class: 'material-icons icon-summary') +
			content_tag(:b, formatted_time)
		end
	end

end
