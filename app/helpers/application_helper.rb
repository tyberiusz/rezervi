module ApplicationHelper
  include CurrentBusinessCart
  include CurrentCart

  def business_cart_count
    puts 'test'
    puts current_business_cart
    puts current_business_cart.id
    puts current_business_cart.total_price
    #if current_business_cart.cart_size > 0
    #  current_business_cart.cart_size
    #end
  end

  def link_off_tbl(name, path)
  	link_to name, path, data: { turbolinks: false }
  end


   def primary_container(&block)
    content_tag :div, { :class => "primary_container" } do
        block.call
      end
   end

  def container(&block)
    content_tag :div, { class: "container" } do 
      block.call
    end
  end
  
  ### BREADCRUMBS

  def root_breadcrumb
    link_to 'rezervi™', root_url
  end

  def breadcrumb(&block)
    container do
      content_tag :div, { :class => "bradcrumb_container" } do
        content_tag :div, { :class => "bradcrumb_scrollable", :itemprop => "breadcrumb" } do
          block.call
        end
      end
    end
  end

  def breadcrumb_item(name = nil, url = nil, html_options = {}, &block)
    if name or block
      html_options[:class] = "#{html_options[:class]} breadcrumb" unless url
      
        if block
          block.call name, url
        else
          url ? link_to(name, url, class: 'breadcrumb') : content_tag(:span, name, {class: 'breadcrumb' } )
        end
      
    end
  end



  ### TIME

  def time_for_h(x)
    x.strftime("%m/%d/%Y %H:%M")
  end

  ### MONTHS
  def month_pl_dop(month_number)
    array = %w[- stycznia lutego marca kwietnia maja czerwca lipca sierpnia września października listopada grudnia]
    #first item in array is -, beacouse first key in date month array is 1
    array[month_number]
  end

  def day_of_week_pl(date)
    days = { sunday: 0, monday: 1, tuesday: 2, wednesday: 3, thursday: 4, friday: 5, saturday: 6 }
    array = %w[niedziela poniedziałek wtorek środa czwartek piątek sobota]
    num = date.strftime("%A").to_s.downcase
    days
    days[date.wday]
    array[date.wday]

  end

  def date_with_month_pl_dop(date)
    month = month_pl_dop(date.month)
    date.strftime("%e #{month} %Y")
  end
  ### PRICE

  def price_for_human(x)
    price = number_to_currency(x, :delimiter => ".", :separator => "," , :format => "%n %u", :unit => "zł")
    #price.to_s << ' zł'
    price
  end

  def price_netto(price_brutto)
    result = price_brutto / 1.23
    price_for_human(result)
  end

  def price_vat(price, from='brutto')
    if from == 'brutto'
      result = (price * 0.23) / 1.23
    elsif from == 'netto'
      result = price * 0.23
    end
    price_for_human(result)
  end

  def price_brutto(price_netto)
    result = price_netto * 1.23
  end
  
  def price_to_h(price)
    price_arr = price.to_s.split(".")
    zlotys = price_arr.first
    gross = price_arr.last
    "<span class='price'>#{zlotys}<span class='grosze'>,#{gross}</span></span>".html_safe
  end

  ### DISCOUNTS 

  def standard_discount(discount)
    result = "<span class='discount badge green' >-#{discount.price}" 
    discount.percent? ? result << "%" : result << "zł" 
    result.html_safe
  end


  def ducat_discount(discount)
    amount = "<span class='grey-text- text-darken-2-' style='color:#a67c00;'>#{discount.price}x </span>"
    dukat = "<span class='dukat z-depth-1 primary-text' style='background-color:yellow-; z-index:3 ;background-image: radial-gradient(#ffbf00 15%, #ffcf40 75%, #ffdc73 90%);  color:dark; border:2px solid #a67c00; border-radius:50% !important;'  >"
    text = "<div class='center-align' style=' font-weight:bold; font-size:15px; position:relative; top:1px;'><i class='material-icons white-text-' style='color:#a67c00;'>loyalty</i><b style='color:#a67c00;'></b></div></span>"
    result = ""
    result << amount if discount.price.to_i > 0
    result << dukat << text
    result.html_safe
  end

  def ducat_heart(discount)
    amount = "<span class='dukat_amount grey-text text-darken-2 hide-'>#{discount.price}x </span>"
    dukat = "<span class='dukat z-depth-1 primary-text' style='background-color:yellow-; z-index:3 ;background-image: radial-gradient(#ffbf00 15%, #ffcf40 75%, #ffdc73 90%);  color:dark; border:2px solid #a67c00; border-radius:50% !important;'  >"
    text = "<div class='center-align' style=' font-weight:bold; font-size:15px; position:relative; top:1px;'><i class='material-icons white-text-' style='color:#a67c00;'>favorite</i><b style='color:#a67c00;'></b></div></span>"
    result = ""
    result << amount if discount.price.to_i > 0
    result << dukat << text
    result.html_safe
  end

  def ducat_heart_count(count)
    amount = "<span class='dukat_amount grey-text text-darken-2 hide-'>#{count}x </span>"
    dukat = "<span class='dukat z-depth-1 primary-text' style='background-color:yellow-; z-index:3 ;background-image: radial-gradient(#ffbf00 15%, #ffcf40 75%, #ffdc73 90%);  color:dark; border:2px solid #a67c00; border-radius:50% !important;'  >"
    text = "<div class='center-align' style=' font-weight:bold; font-size:15px; position:relative; top:1px;'><i class='material-icons white-text-' style='color:#a67c00;'>favorite</i><b style='color:#a67c00;'></b></div></span>"
    result = ""
    result << amount if count.to_i > 0
    result << dukat << text
    result.html_safe
  end

  def affiliate_discount(discount)
    card = '<div id="card" class="white lighten-2 center-align z-depth-1" style="width: 94px;     height: 62px; border-radius:5px; overflow: hidden; position:relative; border:1px solid gray ;display: inline-block; margin-bottom:4px;">'
    img =  '<img  />'
    div =    '<div style="position: absolute;  bottom: 0;  margin: 0 auto;   width:100%; ">'
    badge =   '<div class="discount badge green "  ><i class="material-icons tiny " style="position:relative; top:2px;">local_offer</i>'
    end_badge =  '</div>'
    end_div = '</div>'
    end_card = '</div>'
    result = ""
    result << card << img << div << badge << "-#{discount.price.to_i}%" << end_badge << end_div << end_card
    result.html_safe
  end

  def choose_discount_kind(x)
    puts x.kind
    #puts x
    case x.kind
    when 'standard_discount' then standard_discount(x)
    when 'ducat_discount' then ducat_discount(x)
    when 'ducat_heart' then ducat_heart(x)
    when 'affiliate_discount' then affiliate_discount(x)
    end
    #puts 'x'
  end


  


end