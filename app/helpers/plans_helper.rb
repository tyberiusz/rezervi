module PlansHelper

	def infinity
		content_tag(:i, 'all_inclusive', class: 'material-icons left infinity tooltipped', data: {position:"right", delay:"250", tooltip:"bez limitu" })
		
	end

	def select_all_time_options_plan
		arr ={}
		time_options = Plan::TIME_OPTIONS_ALL.map { |k,v| [v, k] }
		puts 
		#puts time_options
		time_options
	end

	def select_pay_time_options_plan
		puts 'pay_only time_options'
		time_options = Plan::TIME_OPTIONS_PAY.map { |k,v| k[v] }
		#puts time_options #['test'].remove

		time_options

	end


	def translate_time_sub_for_human
		time_options = Plan::TIME_OPTIONS_ALL.map { |k,v| [v, k] }
		puts time_options
		time_options
	end

	
	def price
		
	end

	def in_businees_cart?(plan)
		array = current_business_cart.business_cart_items.pluck(:plan_id)
		array.include?(plan)
	end

	def test_plan?
		current_business_cart.business_cart_items.test.first
	end

	def have_test_period?
		array = current_business_cart.business_cart_items.pluck(:period)
		array.include?('test')
	end
	

end
 