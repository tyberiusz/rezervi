class CartBooking < ApplicationRecord
  belongs_to :cart
  belongs_to :company
  has_many :cart_items

  before_validation :create_hash_of_uniq

  validates_uniqueness_of :hash_of_uniq, allow_blank: true

  def create_hash_of_uniq
  	self.hash_of_uniq = Digest::SHA1.hexdigest([self.cart_id, self.company_id, self.date_of].join)
  	puts hash_of_uniq
  end

  def starts_at
  	self.cart_items.order(starts_at: 'asc').first.starts_at
  end

end
