class OpeningHours < ApplicationRecord
  belongs_to :openingable, polymorphic: true

  enum day_of_week: Date::DAYS_INTO_WEEK.keys.map { |day| day.to_s.capitalize }.freeze
  

  scope :standard, -> { where(standard: true) }
  scope :non_standard, -> { where(standard: false) }

  def today
  	Time.now.strftime("%a")
  end

  def closed?
  	!self.opened?
  end


end
