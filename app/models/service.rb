class Service < ApplicationRecord
  belongs_to :company
  belongs_to :service_group, optional: true
  #has_many :discounts
  belongs_to :booking_discount, class_name: "Discount", foreign_key: 'booking_discount_id', optional: true
  belongs_to :buying_discount, class_name: "Discount", foreign_key: 'buying_discount_id', optional: true
  belongs_to :prepayment_discount, class_name: "Discount", foreign_key: 'prepayment_discount_id', optional: true
  has_many :cart_items, dependent: :destroy

  def duration
  	#calc_duration
    duration_in_min
  end

  def any_discount?
  	 prepayment_discount || buying_discount || booking_discount
  end

  def price_booking_discount
    booking_discount = self.booking_discount
    booking_discount.standard_discount? ?  booking_discount.price : 0
  end

  def price_buying_discount
    buying_discount = self.buying_discount
    buying_discount.standard_discount? ? buying_discount.price : 0
  end

  def price_prepayment_discount
    prepayment_discount = self.prepayment_discount
    prepayment_discount.standard_discount? ? prepayment_discount.price : 0
  end

  private

  #def calc_duration
  #	unless max_duration.present?
  #		min_duration 
  #	else
  #		[min_duration, max_duration]
  #	end
  #end

end
