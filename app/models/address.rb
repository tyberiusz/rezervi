class Address < ApplicationRecord
  belongs_to :city, optional: true;
  #enum kind: [:register, :correspondence]

  validates :street_name, presence: {message: 'Ulica jest wymagana'}
  validates :street_number, presence: {message: 'Nr budynku jest wymagany'}
  validates :zip_code, presence: {message: 'Kod pocztowy jest wymagany'}
  validates :city_name, presence: {message: 'Miasto jest wymagane'}
end
