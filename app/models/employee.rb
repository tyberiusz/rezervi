class Employee < ApplicationRecord
  belongs_to :company

  has_many :opening_hours, as: :openingable

  def full_name
  	"#{first_name} #{last_name}"
  end

  def name_with_job
    [full_name, job_occupation]
  end

  def standard_opening_employee_at(date)
  day_of_week = date.strftime("%A")
    #puts 'company2 - standard_opening_employee_at'
    standard_opening = OpeningHours.standard
    .where(openingable_type: 'Employee', openingable_id: self.id, day_of_week: day_of_week).take
    #standard_opening_arr = []
    #standard opening.each_with_index do |opening_day, i|
    if standard_opening == nil
      return 'zero_standard'
	elsif standard_opening.closed? && standard_opening.all_of_day?
	  return 'closed' 
	else
      [standard_opening.start_time, standard_opening.end_time] if standard_opening.opened?
    end
  end

  def non_standard_opening_employee_at(date)
    non_standard_opening = OpeningHours.non_standard
    .where(openingable_type: 'Employee', openingable_id: self.id, day: date).take

    if non_standard_opening == nil
      return 'zero_non_standard'
    elsif non_standard_opening.closed? && non_standard_opening.all_of_day?
      return 'closed'
    else
      [non_standard_opening.start_time, non_standard_opening.end_time]
    end
  end

  def employee_opening_at(date)
   company_opening = self.company.company_opening_at(date)
    unless company_opening == 'closed'
   		if non_standard_opening_employee_at(date) == 'zero_non_standard' && standard_opening_employee_at(date) == 'zero_standard'
   			self.company.company_opening_at(date)
   		elsif non_standard_opening_employee_at(date) == 'zero_non_standard' && standard_opening_employee_at(date) != 'zero_standard'
   			standard_opening_employee_at(date)
   		elsif non_standard_opening_employee_at(date) != 'zero_non_standard' && standard_opening_employee_at(date) == 'zero_standard'
   			non_standard_opening_employee_at(date)
   		elsif non_standard_opening_employee_at(date) != 'zero_non_standard' && standard_opening_employee_at(date) != 'zero_standard'
   			non_standard_opening_employee_at(date)
   		end
    else
    	return 'closed'
    end
  end


  def avaliable_slots(date)
  	#slots = {}

    unless employee_opening_at(date) == 'closed'
    	selected_date = date.change(day: date.day, month: date.month, year: date.year)
      puts date
      slots_before_midnight = []
      slots_between_midnight_and_sixteen = []
      slots_after_sixteen = []

    	start_slot = employee_opening_at(date).first 
    	end_slot = employee_opening_at(date).last

    	(start_slot.to_i..end_slot.to_i).step(10.minutes) do |date|
        time = Time.at(date).change(day: selected_date.day, month: selected_date.month, year: selected_date.year)
    	 slots_before_midnight << time if time < time.midday && time >= time.beginning_of_day
    	 #puts (time )

       slots_between_midnight_and_sixteen << time if time >= time.midday && time < time.change(hour: 16)
       #puts (time )

       slots_after_sixteen << time if time >= time.change(hour: 16) && time <= time.end_of_day
       #puts (time )
    	end
    	#slots.step(10.minutes
    	slots = { before_midnight: slots_before_midnight, between_midnight_and_sixteen: slots_between_midnight_and_sixteen, after_sixteen: slots_after_sixteen } 
    else
      'zero'
    end
  end





end
