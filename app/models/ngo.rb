class Ngo < ApplicationRecord

  #require 'addressable/uri'

  belongs_to :address
  belongs_to :correspondence_address, class_name: 'Address', optional: true
  belongs_to :city, optional: true

  accepts_nested_attributes_for :address

  enum ambassador_status: [:candidate, :ambassador]

  validates :full_name, presence: {message: 'Nazwa organizacji jest wymagana'}
  validates :email, presence: {message: 'Email jest wymagany'}
  validates :krs_number, presence: {message: 'KRS jest wymagany'}
  validates :nip_number, presence: {message: 'NIP jest wymagany'}
  validates :regon_number, presence: {message: 'REGON jest wymagany'}
  validates :address, presence: {message: 'Adres jest wymagany'}


  

#email
=begin
 full_name
email
krs_number
nip_number
regon_number
opp
business
future_business_possible
ambassador_status
=end
end
