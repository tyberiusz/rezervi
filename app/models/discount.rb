class Discount < ApplicationRecord

	enum kind: [:standard_discount, :affiliate_discount, :ducat_discount, :ducat_heart, :ducat_gift ]
	enum discount_for: [:booking, :buying, :prepayment]
	#belongs_to :service
	has_many :services
end
