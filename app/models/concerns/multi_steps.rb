module MultiSteps
  extend ActiveSupport::Concern

  included do
    
    attr_writer :current_step

    #validates_presence_of :shipping_name, :if => lambda { |o| o.current_step == "shipping" }
    #validates_presence_of :billing_name, :if => lambda { |o| o.current_step == "billing" }

    #def current_step
    #  @current_step || steps.first
    #end

    #def steps
    #  %w[shipping billing confirmation]
    #end
    COUNT_OF_STEPS = {
      "1" => 'one-step',
      "2" => 'two-step',
      "3" => 'three-step',
      "4" => 'four-step'
    }.freeze

    def size_of_steps
      avaliable_steps.size
    end

    def length_of_step
      COUNT_OF_STEPS[size_of_steps.to_s]
    end

    def next_step
      #puts self.current_step.inspect
      #puts  steps[steps.index(self.current_step)]
      #puts steps
      #avaliable_steps.index(avaliable_steps[self.current_step]).to_i + 1
      avaliable_steps.index(self.this_step) + 1
      #self.current_step = steps[steps.index(current_step)+1]
    end

    def avaliable_steps
      steps.first.keys
    end

    def previous_step
      #self.current_step = steps[steps.index(current_step)-1]
      #steps.index(steps[self.current_step]).to_i - 1
      avaliable_steps.index(self.this_step) - 1
    end

    def first_step?
      avaliable_steps[current_step] == avaliable_steps.first
    end

    def last_step?
      current_step == avaliable_steps.last
    end

    def in_progress?
      'in-progress'
    end

    def done?
      'done'
    end

    def show_back_button?
      size_of_steps > 1 && !first_step?
    end

    def show_next_button?
      size_of_steps > 1 && !last_step?
    end

    def step_state(step)
      #unless self.current_step
        #if step.index
       #if @cart_item.current_step == steps[step]
       # puts true
       #end
        #end
    end



    def all_valid?
      avaliable_steps.all? do |step|
        self.current_step = step
        valid?
      end
    end 

  end
end