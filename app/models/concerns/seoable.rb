module Seoable
  extend ActiveSupport::Concern

  included do
    before_validation :create_seo
    has_one :seo, dependent: :destroy, as: :seoable
    before_update :need_update_seo?
    after_update :update_seo_slug
    #delegate :slug, to: :seo, prefix: :setting
    #line above is unnecessary
    
    #Model.constantize.reflect_on_all_associations

    def name_model
    #  puts self.class.name.constantize
      self.class.name.constantize
    #  self.class.name.constantize
    end

    def has_any_company?
      self.companies.any? 
    end

    def update_all_companies_slug
      self.companies.each { |company| company.seo.update_slug }
    end



    private
      def create_seo
        self.seo = Seo.new unless seo.present?
      end

      def need_update_seo?
        @need_update = name_changed?
      end

      def update_seo_slug
        seo.update_slug if @need_update
      end

  end
end