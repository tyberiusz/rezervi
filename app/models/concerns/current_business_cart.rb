# models/concerns/current_business_cart.rb

module CurrentBusinessCart

  private

    def set_business_cart
      @business_cart = BusinessCart.find(session[:business_cart_id])
    rescue ActiveRecord::RecordNotFound
      @business_cart = BusinessCart.create
      session[:business_cart_id] = @business_cart.id
    end

    def current_business_cart
      if !session[:business_cart_id].nil?
        BusinessCart.find(session[:business_cart_id])
      end
	end

end