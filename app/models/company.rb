class Company < ApplicationRecord
  include Seoable

  belongs_to :category
  belongs_to :city
  has_many :services_groups, -> { order(:order => :asc) }
  has_many :services, through: :services_groups

  has_many :employees
  has_many :openingable

  has_many :partnerships
  has_many :partners, through: :partnerships

  has_many :inverse_partnerships, :class_name => "Partnership", :foreign_key => "partner_id"
  has_many :inverse_partners, :through => :inverse_partnerships, :source => :company

  has_many :opening_hours, as: :openingable

  def all_partners
  	partners + inverse_partners 
  end

  def address
  	result = short_address  	
  	result << ", #{postal_code} #{city.name}"
  end

  def short_address
  	result = ""
  	result << "#{street} #{street_number}"
  	result << "/#{flat_number}" unless flat_number.blank?
  	result
  end



  def standard_opening_company_at(date)
  day_of_week = date.strftime("%A")
    #puts 'company2 - standard_opening_company_at'
    standard_opening = OpeningHours.standard
    .where(openingable_type: 'Company', openingable_id: self.id, day_of_week: day_of_week).take
    #standard_opening_arr = []
    #standard opening.each_with_index do |opening_day, i|
    return 'closed' if standard_opening.closed? && standard_opening.all_of_day? || standard_opening == nil
    [standard_opening.start_time, standard_opening.end_time] if standard_opening.opened?
    #end
  end

  def non_standard_opening_company_at(date)
    puts non_standard_opening = OpeningHours.non_standard
    .where(openingable_type: 'Company', openingable_id: self.id, day: date).take

    if non_standard_opening == nil
      return 'zero_non_stadard'
    elsif non_standard_opening.closed? && non_standard_opening.all_of_day?
      return 'closed'
    else
      [non_standard_opening.start_time,non_standard_opening.end_time]
    end
  end

  def company_opening_at(date)
    if non_standard_opening_company_at(date) != 'zero_non_stadard' 
      non_standard_opening_company_at(date) 
    else 
      standard_opening_company_at(date)
    end
  end

  def calculate_opening_hours(date_str=Date.today)
    date = Date.parse(date_str)
    arr_of_regules = self.opening_hours.standard.where(day_of_week: date.wday)
    #all_of_day = false
    puts arr_of_regules
    #arr_of_regules.each do |regule|
      if arr_of_regules.size == 0
        result = 'closed'
      elsif arr_of_regules.size == 1
        if arr_of_regules.first.opened? 
          result = 'opened'
          hours = [arr_of_regules.first.start_time, arr_of_regules.first.end_time]
        else
          result = 'closed'
        end
      end
        #regule.opened? && regule.all_of_day?
        #result = ["00:00:00", "00:00:00"]
      #elsif regule.opened?
        #result = [regule.start_time, regule.end_time]
      #end
    puts result
    puts hours
    #end
  end

end
