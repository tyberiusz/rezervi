class Seo < ApplicationRecord

  extend FriendlyId
  EXCLUDED_SLUG_VALUES = %w(users articles authors topics admin your all rezervi biznes)
  friendly_id_config.reserved_words.concat(EXCLUDED_SLUG_VALUES)

  friendly_id :set_candidates, use: [:slugged , :history]
  belongs_to :seoable, polymorphic: true
  validates :slug, presence: true, uniqueness: { case_sensitive: false }

  
  def should_generate_new_friendly_id?
    true
  end

  def update_slug
    self.should_generate_new_friendly_id?
    self.save!
  end

  private

  # Generate the slug based on the name of the Sluggable class
  def set_candidates
    case seoable_type
    when 'Company' then company_candidates
    when 'City' then name_candidate
    when 'Category' then name_candidate
    end
  end

  def company_candidates
    [ 
      seoable.name,
      ["#{seoable.name} w mieście #{seoable.city.name} na ulicy #{seoable.street}"]
    ]
  end

  def name_candidate
    [ seoable.name ]
  end


end
