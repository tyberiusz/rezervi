class Plan < ApplicationRecord

  before_destroy :not_referenced_by_any_business_cart_item
  belongs_to :manager, optional: true
  has_many :business_cart_items
  has_many :subscriptions

  validates :price, numericality: { only_integer: true }, length: { maximum: 7 }

  TIME_OPTIONS_ALL = {'test' => '14 dni testu',
   '1month' => '1 miesiąc',
   '3months' => '3 miesiące (-5%)',
   '6months' => '6 miesięcy (-10%)',
   '12months' => '12 miesięcy (-20%)' }.freeze

   TIME_OPTIONS_PAY = {
   '1month' => '1 miesiąc',
   '3months' => '3 miesiące',
   '6months' => '6 miesięcy',
   '12months' => '12 miesięcy' }.freeze
  


  private

   def not_referenced_by_any_business_cart_item
    unless business_cart_items.empty?
      errors.add(:base, 'Plan należy do czyjegoś koszyka')
      throw :abort
    end
   end
	
  
end
