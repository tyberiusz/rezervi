class Lead < ApplicationRecord
  if Rails.env == "development"    
    require "watir"
    require 'nokogiri'
    require 'open-uri'
    
    enum status: [:add, :sent]


    def self.start_scr(from)
      City.where("id > ?", from).each do |city| 
        puts ">>>>>> start scrapping #{city.name} id #{city.id}"
        self.get_links(city.name, city.id)
        puts "<<<<<< scrapped #{city.name}"
        puts ''
      end
    end

    def self.to_url(str)
      url = URI::encode(str)
    end

    def self.get_links(city=nil, city_id=nil)
      #city = "Zbąszynek"        &cat[2384]=2783&cat[2386]=2387&cat[2898]=2899
      url = "https://spis.ngo.pl/?cat[2384]=2783&cat[2386]=2387&cat[2898]=2899&search=#{city}" 
      url = self.to_url(url)      
      #url2 = 'https://spis.ngo.pl/?cat[2384]=2783&cat[2898]=2899&search=Zb%C4%85szynek'
      city_id = City.find_by(name: city).id
      
      page = Nokogiri::HTML(open(url).read)
      page.encoding = 'utf-8'
      #puts page.css(".searchString").content
      puts page.css(".searchString")[0]['value']
      page.css(".bahama-blue").each do |ngo|
        puts ngo.text.strip 
        puts ngo_link = ngo['href']
        if ngo_link != '#filter' && ngo_link != "" && ngo_link != nil && ngo_link != 'https://spis.ngo.pl/dodaj'
          data = self.get_email(ngo_link)
          if data.size == 2
            title = data.first
            email = data.last
            unless email == 'no-link'
              Lead.create(name: title, email: email, city_id: city_id)
            end
         end
        end
        #ngo = 
        #browser = Watir::Browser.new
        #browser.goto ngo_link
        #doc = Nokogiri::HTML.parse(browser.html)

      end
    end

    def self.get_email(url)
        #url1 = 'https://spis.ngo.pl'
        #url2 ="https://spis.ngo.pl/200255-polskie-towarzystwo-turystyczno-krajoznawcze-oddzial-pttk-pleszew"
        browser = Watir::Browser.new
        browser.goto url
        
        title = browser.div(class: ["tc", "f6", "fw5"]).text
        puts title
        col = browser.div(class: ["tr", "grow-1"])
        span = col.span(class: ["underline", "bahama-blue", "no-underline-hover", "pointer"])
        if span.exists? #puts span.text
          span.click
          browser.wait_until(10)

          email = col.link(class: 'bahama-blue').text
          #puts link.exist?
          #puts email.text
        else
          email = 'no-link'
        end
        browser.close
        puts [title, email]
        [title, email]
        #doc = Nokogiri::HTML.parse(browser.html)
    end
  end
end
