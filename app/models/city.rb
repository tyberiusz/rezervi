class City < ApplicationRecord
    require 'nokogiri'
    require 'open-uri'
	has_many :addresses

	include Seoable
	enum voivodeship: 
	["dolnośląskie",
    "kujawsko-pomorskie",
    "lubelskie",
    "lubuskie",
    "łódzkie",
    "małopolskie",
    "mazowieckie",
    "opolskie",
    "podkarpackie",
    "podlaskie",
    "pomorskie",
    "śląskie",
    "świętokrzyskie",
    "warmińsko-mazurskie",
    "wielkopolskie",
    "zachodniopomorskie"]

	#validates :name, :voivodeship, presence: true
	has_many :companies, dependent: :nullify


    scope :less_1k, -> { where("amount < ?", 1000) }
    scope :over_1k, -> { where(" amount >= ? and amount < ?", 1000, 2500) }
    scope :over_2_5k, -> { where(" amount >= ? and amount < ?", 2500, 5000) }
    scope :over_5k, -> { where(" amount >= ? and amount < ?", 5000, 10000) }
    scope :over_10k, -> { where(" amount >= ? and amount < ?", 10000, 25000) }
    scope :over_25k, -> { where(" amount >= ? and amount < ?", 25000, 50000) }
    scope :over_50k, -> { where(" amount >= ? and amount < ?", 50000, 100000) }
    scope :over_100k, -> { where(" amount >= ? and amount < ?", 100000, 250000) }
    scope :over_250k, -> { where(" amount >= ? and amount < ?", 250000, 500000) }
    scope :over_500k, -> { where(" amount >= ? and amount < ?", 250000, 500000) }

    def self.scrapping_city

        url = "https://pl.wikipedia.org/wiki/Dane_statystyczne_o_miastach_w_Polsce"

        page = Nokogiri::HTML(open(url))   
        #puts @page.class   # => Nokogiri::HTML::Document
        table = page.css("table.wikitable").search('tbody')

        rows = table.css('tr').map do |row|
            unless row == nil
            city = City.new
            city_name = row.search('td')[0]
            city.name = city_name.css('a').text unless city_name.blank?

            city_district = row.search('td')[1]
            city.district = city_district.css('a').text.to_s.gsub("[a]", "") unless city_district.blank?

            city_voivodeship = row.search('td')[2]
            city.voivodeship = city_voivodeship.css('a').text.to_s.downcase  unless city_voivodeship.blank?

            city_amount = row.search('td')[4]
            city.amount = city_amount.text.to_i unless city_amount.blank?

            puts "#{city.name} - #{city.district} - #{city.voivodeship} - #{city.amount} "
            city.save!
            end
        end
    end

end




    
