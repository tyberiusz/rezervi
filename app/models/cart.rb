class Cart < ApplicationRecord
	include MultiSteps
	has_many :cart_items,  dependent: :destroy
	has_many :cart_bookings,  dependent: :destroy
	enum kind_of_booking: [:booking, :buying, :prepayment]

	def calucalte_discount
		#self.kind_of_booking
	end

	def cart_total
		sum = 0
		self.cart_items.each do |cart_item|
			cart_item.price_with_avaliable_discounts
			sum += cart_item.price_with_avaliable_discounts
		end
		sum.to_f
		puts self.id
		puts sum

		sum
		
	end

	def cart_size
	  priv_cart_size
	end

	def cart_discount_heart_count
		count = 0
		self.cart_items.each do |cart_item|
			ducat_heart = cart_item.service.booking_discount
			if ducat_heart.ducat_heart?
				count += ducat_heart.price
			end
		end
		puts count
		count
	end

	def cart_discount_affiliate_count
		count = 0
		self.cart_items.each do |cart_item|
			if self.booking?
				count += 1 if cart_item.service.booking_discount.affiliate_discount?
			elsif self.buying?
				count += 1 if cart_item.service.booking_discount.affiliate_discount?
				count += 1 if cart_item.service.buying_discount.affiliate_discount?
			elsif self.prepayment?
				count += 1 if cart_item.service.booking_discount.affiliate_discount?
				count += 1 if cart_item.service.buying_discount.affiliate_discount?
				count += 1 if cart_item.service.prepayment_discount.affiliate_discount?
			end
			#if ducat_heart.ducat_heart?
			#	count += ducat_heart.price
			#end
		end
		count
	end

	def cart_discount_standard_price
		discount = 0
		self.cart_items.each do |cart_item|
			puts cart_item.avaliable_discount
			discount += cart_item.avaliable_discount
		end
		puts discount
		discount
	end

	def cart_max_standard_discount
		discount = 0
		self.cart_items.each do |cart_item|
			puts cart_item.max_discount
			discount += cart_item.max_discount
		end
		puts discount
		discount
	end


	private

	def priv_cart_size
		cart_items.configured.size
	end

end
