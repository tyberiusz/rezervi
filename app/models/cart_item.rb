class CartItem < ApplicationRecord
  
  include MultiSteps
  attr_writer :current_step

  belongs_to :cart
  belongs_to :service 
  belongs_to :employee, optional: true

  enum kind_of_booking: [:booking, :prepayment]
  #enum current_step: [:employee, :datetime]
  before_validation :set_random_employee
  after_create :set_kind_of_booking_on_cart
  #before_save :update
  before_update :set_configured
  
  scope :configured, ->  { where(configured: true)}
 # with_options if: => lambda { |o| o.current_step == "select_employee" } do |item|
  #  item.custom_employee if employee_id == 0 || employee_id == nil
 #   item.validates_presence_of :employee_id
#end

  validates :employee_id, presence: true, :if => lambda { |o| o.current_step == "select_employee" }
  validates :starts_at, presence: true, :if => lambda { |o| o.current_step == "select_employee" }
  
  def price_with_avaliable_discounts
    service = self.service
    cart = self.cart
    case cart.kind_of_booking
    when 'booking'
      price = service.price - service.price_booking_discount
    when 'buying'  
      price = service.price - service.price_booking_discount - service.price_buying_discount
    when 'prepayment'
      price = service.price - service.price_booking_discount - service.price_buying_discount - service.price_prepayment_discount
    end
    price
  end

  def avaliable_discount
    self.service.price - self.price_with_avaliable_discounts
  end

  def max_discount
    service = self.service
    discount = service.price_booking_discount + service.price_buying_discount + service.price_prepayment_discount
  end

  

  def steps
    [
      select_employee: {name: 'Pracownik', icon: 'person'},
      select_datetime: {name: 'Termin', icon: 'event'} 

    ]
  end

  def step_index(custom)
    self.avaliable_steps.index(custom)
  end

  def this_step
    self.avaliable_steps[self.current_step]
  end

  def current_step_index
    self.current_step
  end
  

  
  ### method to calculate avaliable time to reservation

  def duration_of_service
    self.service.duration #in minutes
  end

  def check_avaliable_opening_hours(date) 
    unless self.service.company.company_opening_at(date) == 'closed'
      unless self.employee.employee_opening_at(date) == 'closed'
        'open'
      end
    end
  end 

  private

  def set_random_employee
    if self.employee_id == 0 #&& steps[self.current_step] == 'select_employee'
     self.employee_id = self.service.company.employee_ids.sample 
    end
  end

  def can_choose_employee?
    self.service.company.employee_ids.size > 1
  end

  def set_kind_of_booking_on_cart
    self.cart.update_attribute(:kind_of_booking, :prepayment) if self.kind_of_booking == 'prepayment'
  end

  def set_configured
    #puts 'configured?'
    puts "self.this_step: #{self.this_step}"
    puts "self.last_Step: #{self.avaliable_steps.last}"
    #puts self.configured?
    puts "self.configured? #{self.configured?}"

    puts "starts_at #{self.starts_at}"
    puts "starts_at.blank? #{self.starts_at.blank?}"
    puts "starts_at.nil? #{self.starts_at == nil}"
    puts "self.valid? #{valid?}"
    puts "self.errors #{self.errors.messages}"
    puts "1 #{self.this_step == avaliable_steps.last}"
    puts "2 #{self.valid?}"
    puts "avaliable_steps: #{avaliable_steps}"
    if self.valid? && self.this_step == avaliable_steps.last
      #puts "self.configured? #{self.configured?}"
    elsif self.valid?
      #puts "current step in out: #{self.current_step = self.next_step}"
      #self.update_attribute(:configured, true)
      #self.current_step = avaliable_steps[self.next_step]
      #self[:current_step] = avaliable_steps[self.next_step]
      #self.attributes = {current_step: avaliable_steps[self.next_step]}
      #puts self.next_step
      #puts "current step: #{self.current_step = self.next_step}"
    else
      #puts "self.configured? #{self.configured?}"
      #self.configured = false
    end
     #puts "starts_at #{self.starts_at}"
     #puts "starts_at.blank? #{self.starts_at.blank?}"
     #puts self.current_step
  end

end
