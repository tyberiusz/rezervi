class Category < ApplicationRecord

	include Seoable
	validates :name, presence: true
	has_many :companies, dependent: :nullify
	
end
