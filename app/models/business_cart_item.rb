class BusinessCartItem < ApplicationRecord
  belongs_to :business_cart
  belongs_to :plan
  belongs_to :subscription

  def total_price
    #plan.price * quantity
    subscription.total_price * quantity
  end

  #def price_for_human
  #	number_with_precision(total_price, :precision => 2, :delimiter => ',')
  #end

  #scope :test, -> { where(period: 'test') }
end
