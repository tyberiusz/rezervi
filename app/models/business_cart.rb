class BusinessCart < ApplicationRecord
	has_many :business_cart_items,  dependent: :destroy
  accepts_nested_attributes_for :business_cart_items
  
  def add_product(product, quantity, subscription)
    
     quantity = quantity.to_i
     current_item = business_cart_items.find_by(plan_id: product.id, subscription_id: subscription.id)
     @subscription = subscription
     one_test_period_only
    if current_item
       #current_item.quantity += quantity
    else
       current_item = business_cart_items.build(
        plan_id: product.id,
        quantity: quantity,
        subscription: @subscription
        )
    end
     current_item
  end

  def one_test_period_only
    #if test_period_in_business_cart?
    #  @period = '1month'
    #end
  end

  def test_period_in_business_cart?
   # x = business_cart_items.pluck(:period).include? 'test'
   # puts x 
    #x
  end

  def total_price
    business_cart_items.to_a.sum { |item| item.total_price }
  end

  def cart_size
     size = 0
     self.business_cart_items.each do |cart_item|
     size += cart_item.quantity
    end
     size
  end

  
end
