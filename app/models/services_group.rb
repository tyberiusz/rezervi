class ServicesGroup < ApplicationRecord
  belongs_to :company
  has_many :services
end
