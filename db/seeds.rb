# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Plan.create([name: 'Starter', price: 35, sms_limit: 50, email_limit:250])
Plan.create([name: 'Bussiness', price: 55, sms_limit: 100, email_limit: 500])
Plan.create([name: 'Enterpriser', price: 95, sms_limit: 200, email_limit:1000 ])
Plan.create([name: 'Ultimate', price: 145, sms_limit: 500, email_limit: 2500])

Plan.first.subscriptions.create(time: 'test', discount: 100, price:0)
Plan.first.subscriptions.create(time: '1month', discount: nil, price:42)
Plan.first.subscriptions.create(time: '3months', discount: 5, price:40)
Plan.first.subscriptions.create(time: '6months', discount: 10, price:40)
Plan.first.subscriptions.create(time: '12months', discount: 20, price:35)

City.create(name: 'Pleszew')
City.create(name: 'Chrzanów')
City.create(name: 'Gorzów Wielkopolski')

Category.create(name: 'Fryzjer')
Category.create(name: 'Salony kosmetyczne')

Company.create(name: 'Salon fryzjerski Aleksandra',
			   city_id: 2,
			   category_id: 1,
			   postal_code: '32-500',
			   street: 'Biedronkowa',
			   street_number: '15',
			   flat_number: '2',
			   description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer scelerisque nec arcu id bibendum. Praesent eu nibh tempor, gravida augue mollis, tristique eros. Vestibulum aliquam ante nisl, id pharetra enim feugiat id. Nunc sollicitudin hendrerit mi, et lacinia nibh commodo vel. Aliquam gravida nunc sed dui accumsan interdum. Vestibulum in leo nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempor tempus diam, non accumsan nisl rhoncus eget. Fusce dui leo, mattis quis eros vel, vestibulum hendrerit elit. Fusce eget congue metus. Morbi sed dolor sed enim eleifend consectetur. Morbi ac nisi vitae orci imperdiet suscipit. Proin purus nibh. "
			   )

OpeningHours.create(
start_time: '8:00',
end_time: '16:00',
day_of_week: 0,
all_of_day: false,
opened:true,
standard:true,
openingable_type: 'Company',
openingable_id: Company.first.id)


OpeningHours.create(
start_time: '8:00',
end_time: '16:00',
day_of_week: 1,
all_of_day: false,
opened:true,
standard:true,
openingable_type: 'Company',
openingable_id: Company.first.id)

OpeningHours.create(
start_time: '8:00',
end_time: '16:00',
day_of_week: 2,
all_of_day: false,
opened:true,
standard:true,
openingable_type: 'Company',
openingable_id: Company.first.id)

OpeningHours.create(
start_time: '12:00',
end_time: '20:00',
day_of_week: 3,
all_of_day: false,
opened:true,
standard:true,
openingable_type: 'Company',
openingable_id: Company.first.id)

OpeningHours.create(
start_time: '8:00',
end_time: '12:00',
day_of_week: 4,
all_of_day: false,
opened:true,
standard:true,
openingable_type: 'Company',
openingable_id: Company.first.id)


OpeningHours.create(
start_time: '8:00',
end_time: '12:00',
day_of_week: 5,
all_of_day: true,
opened:false,
standard:true,
openingable_type: 'Company',
openingable_id: Company.first.id)

OpeningHours.create(
start_time: '00:00',
end_time: '00:00',
day_of_week: 6,
all_of_day: true,
opened:false,
standard:true,
openingable_type: 'Company',
openingable_id: Company.first.id)

OpeningHours.create(
day: Time.now + 7.days,
start_time: '8:00',
end_time: '10:00',
day_of_week: 5,
all_of_day: true,
opened:true,
standard:false,
openingable_type: 'Company',
openingable_id: Company.first.id)


Employee.create(first_name: 'Anna', last_name: 'Kowalska', job_occupation: 'Fryzjerka', description: 'Jestem fryzjerką od 7 lat. Uwielbiam moją pracę.', company_id: Company.first.id)
Employee.create(first_name: 'Marcin', last_name: 'Nowak', job_occupation: 'Fryzjer męski', description: 'Kreatywne strzyżenie męskie to moja pasja', company_id: Company.first.id)
Employee.create(first_name: 'Natalia', last_name: 'Nowak', job_occupation: 'Właściciel salonu/ Fryzjerka', description: 'Chcesz zmieniać świat? Zacznij od siebie', company_id: Company.first.id, owner:true,  receive_reservation: false)



OpeningHours.create(
day: nil,
start_time: '8:00',
end_time: '12:00',
day_of_week: 1,
all_of_day: false,
opened:true,
standard:true,
openingable_type: 'Employee',
openingable_id: Employee.first.id)

OpeningHours.create(
day: nil,
start_time: '8:00',
end_time: '12:00',
day_of_week: 2,
all_of_day: false,
opened:true,
standard:true,
openingable_type: 'Employee',
openingable_id: Employee.first.id)

OpeningHours.create(
day: nil,
start_time: '8:00',
end_time: '16:00',
day_of_week: 4,
all_of_day: false,
opened:true,
standard:true,
openingable_type: 'Employee',
openingable_id: Employee.first.id)

OpeningHours.create(
day: Time.now + 7.days,
start_time: '10:00',
end_time: '14:00',
day_of_week: 4,
all_of_day: false,
opened:true,
standard:false,
openingable_type: 'Employee',
openingable_id: Employee.first.id)


ServicesGroup.create(name: 'Usługi fryzjerskie', company_id: Company.last.id, order:2)
ServicesGroup.create(name: 'Paznokcie', company_id: Company.last.id, order:1)

Discount.create( 
				price: 4,
				percent:true, 
				discount_for: :buying,
				kind: :standard_discount
				)

Discount.create( 
				price: 2, 
				discount_for: :prepayment,
				kind: :affiliate_discount
				)
Discount.create( 
				price: 1, 
				discount_for: :booking,
				kind: :ducat_heart
				)

Service.create(
	name: 'Przedłużanie rzęs metodą 3:1',
	description: 'Na jedną naturalną rzęs przypadają 3 sztuczne',
	kind: 0,
	duration_in_min: 20,
	#max_duration: Time.new(0) + 90.minutes,
	price: 123.45,
	company_id: Company.first.id,
	services_group_id: ServicesGroup.first.id,
	booking_discount_id: 3,
	buying_discount_id: 2,
	prepayment_discount_id: 1
	)
Service.create(
	name: 'Przedłużanie rzęs metodą 2:1',
	description: 'Na jedną naturalną rzęs przypadają 2 sztuczne',
	kind: 0,
	duration_in_min: 40,
	#max_duration: Time.new(0) + 100.minutes,
	price: 123.45,
	company_id: Company.first.id,
	services_group_id: ServicesGroup.first.id,
	booking_discount_id: 3,
	buying_discount_id: 2,
	prepayment_discount_id: 1
	)
Service.create(
	name: 'Przedłużanie rzęs metodą 1:1',
	description: 'Na jedną naturalną rzęs przypada 1 sztuczna',
	kind: 0,
	duration_in_min: 60,
	#max_duration: Time.new(0) + 110.minutes,
	price: 123.45,
	company_id: Company.first.id,
	services_group_id: ServicesGroup.first.id,
	booking_discount_id: 3,
	buying_discount_id: 2,
	prepayment_discount_id: 1
	)

Company.create(name: 'Salon kosmetyczny Anna',
			   city_id: 2,
			   category_id: 2,
			   postal_code: '32-500',
			   street: 'Leśna',
			   street_number: '16A',
			   description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer scelerisque nec arcu id bibendum. Praesent eu nibh tempor, gravida augue mollis, tristique eros. Vestibulum aliquam ante nisl, id pharetra enim feugiat id. Nunc sollicitudin hendrerit mi, et lacinia nibh commodo vel. Aliquam gravida nunc sed dui accumsan interdum. Vestibulum in leo nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempor tempus diam, non accumsan nisl rhoncus eget. Fusce dui leo, mattis quis eros vel, vestibulum hendrerit elit. Fusce eget congue metus. Morbi sed dolor sed enim eleifend consectetur. Morbi ac nisi vitae orci imperdiet suscipit. Proin purus nibh. "
			   )

#Partnership.create(company_id: Company.first.id, 
#				   partner_id: Company.last.id)