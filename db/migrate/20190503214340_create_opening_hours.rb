class CreateOpeningHours < ActiveRecord::Migration[5.2]
  def change
    create_table :opening_hours do |t|
      t.date :day
      t.time :start_time, null:false, default: '00:00:00'
      t.time :end_time, null:false, default: '00:00:00'
      t.integer :day_of_week, default: 0
      t.boolean :all_of_day, null:false, default: false
      t.boolean :opened, null:false, default: false
      t.boolean :standard, null:false, default: false
      t.references :openingable, polymorphic: true

      t.timestamps
    end
  end
end
