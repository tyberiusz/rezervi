class CreatePlans < ActiveRecord::Migration[5.1]
  def change
    create_table :plans do |t|
      t.decimal :price, precision: 6, scale: 2
      t.string :name
      t.integer :sms_limit, default: 0
      t.integer :email_limit, default: 0

      t.timestamps
    end
  end
end
