class CreateServicesGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :services_groups do |t|
      t.string :name
      t.integer :order
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
