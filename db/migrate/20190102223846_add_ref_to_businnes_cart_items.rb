class AddRefToBusinnesCartItems < ActiveRecord::Migration[5.2]
  def change
    add_reference :business_cart_items, :subscription, foreign_key: true
  end
end
