class AddReferencesDicountServices < ActiveRecord::Migration[5.2]
  def change
  	add_foreign_key :services, :discounts, column: :booking_discount_id, primary_key: :id
  	add_foreign_key :services, :discounts, column: :buying_discount_id, primary_key: :id
  	add_foreign_key :services, :discounts, column: :prepayment_discount_id, primary_key: :id
  end
end
