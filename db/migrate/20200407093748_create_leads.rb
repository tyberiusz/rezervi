class CreateLeads < ActiveRecord::Migration[5.2]
  def change
    create_table :leads do |t|
      t.string :email
      t.string :name
      t.integer :status
      t.references :city
      t.string :category
      t.timestamps
    end
  end
end
