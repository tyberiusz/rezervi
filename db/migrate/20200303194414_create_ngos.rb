class CreateNgos < ActiveRecord::Migration[5.2]
  def change
    create_table :ngos do |t|
      t.string :full_name
      t.string :email
      t.string :krs_number
      t.string :nip_number
      t.string :regon_number
      t.integer :register_year
      t.boolean :opp
      t.boolean :business
      t.boolean :future_business_possible
      t.references :address, foreign_key: true
      t.references :correspondence_address, class_name: 'Address'
      t.references :city, foreign_key: true
      t.integer :ambassador_status

      t.timestamps
    end
  end
end
