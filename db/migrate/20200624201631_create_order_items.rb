class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.references "order"
	  t.references "service"
	  t.references "employee"
	  t.integer "kind_of_booking", default: 0, null: false
	  t.integer "current_step", default: 0, null: false
	  t.boolean "configured", default: false, null: false
	  t.datetime "starts_at"
	  t.references "booking_order"
	  
	  # copy from service
	  t.datetime :start_duration
      t.datetime :end_duration
      t.integer :duration_in_min
      t.decimal :price, :precision => 10, :scale => 2
      
      t.decimal :price_save, :precision => 10, :scale => 2 #oszcędności
      t.decimal :price_with_discounts, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
