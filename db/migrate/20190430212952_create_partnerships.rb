class CreatePartnerships < ActiveRecord::Migration[5.2]
  def change
    create_table :partnerships do |t|
      t.references :company, foreign_key: true
      #t.references :partner, foreign_key: true

      t.timestamps
    end
  end
end
