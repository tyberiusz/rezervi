class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    #drop_table :companies
    create_table :companies do |t|
      t.string :name
      t.text :description
      #t.references :category, foreign_key: true
      #t.references :city, foreign_key: true
      t.integer :NIP
      t.string :postal_code
      t.string :street
      t.string :street_number
      t.integer :flat_number

      t.timestamps
    end
  end
end
