class CreateBusinessCartItems < ActiveRecord::Migration[5.1]
  def change
    create_table :business_cart_items do |t|
      t.references :business_cart, foreign_key: true
      t.references :plan, foreign_key: true
      t.integer :quantity, default: 1
      t.string :period

      t.timestamps
    end
  end
end
