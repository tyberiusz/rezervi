class AddCartBookingToCartItems < ActiveRecord::Migration[5.2]
  def change
    add_reference :cart_items, :cart_booking, foreign_key: true
  end
end
