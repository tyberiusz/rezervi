class CreateSeos < ActiveRecord::Migration[5.2]
  def change
    drop_table :seos, if_exists: true
    create_table :seos do |t|
      t.string :slug
      t.references :seoable, polymorphic: true
      t.timestamps
    end
  end
end
