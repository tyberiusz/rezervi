class CreateCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_items do |t|
      t.references :cart, foreign_key: true
      t.references :service, foreign_key: true
      t.references :employee, foreign_key: true
      t.integer :kind_of_booking, null: false, default: 0
      t.integer :current_step, null:false, default: 0
      t.boolean :configured, null:false, default: false
      t.datetime :starts_at
      t.timestamps
    end
  end
end
