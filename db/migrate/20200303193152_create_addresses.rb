class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :street_name
      t.string :street_number
      t.string :flat_number
      t.string :zip_code
      t.string :city_name
      t.references :city, foreign_key: true
      t.integer :kind

      t.timestamps
    end
  end
end
