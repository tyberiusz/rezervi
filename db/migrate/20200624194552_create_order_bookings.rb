class CreateOrderBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :order_bookings do |t|
      t.references "order"
      t.references "company"
      t.date "date_of"
      t.string "hash_of_uniq"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.timestamps
    end
  end
end
