class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :name
      t.text :description
      t.integer :kind
      t.datetime :start_duration
      t.datetime :end_duration
      t.integer :duration_in_min
      t.decimal :price, :precision => 10, :scale => 2
      t.references :company, foreign_key: true
      t.references :services_group, foreign_key: true
      t.references :booking_discount
      t.references :buying_discount
      t.references :prepayment_discount
      t.timestamps
    end
  end
  #add_foreign_key :services, :discounts, column: :booking_discount_id, primary_key: :id
  #add_foreign_key :services, :discounts, column: :buying_discount_id, primary_key: :id
  #add_foreign_key :services, :discounts, column: :prepayment_discount, primary_key: :id
end
