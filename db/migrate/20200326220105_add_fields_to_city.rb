class AddFieldsToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :district, :string
    add_column :cities, :voivodeship, :integer
    add_column :cities, :amount, :bigint
  end
end
