class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.string :time
      t.decimal :price, precision: 6, scale: 2, default: 0
      t.boolean :discount_percent
      t.decimal :discount, precision: 6, scale: 2, default: 0
      t.references :plan
      t.timestamps
    end
  end
end
