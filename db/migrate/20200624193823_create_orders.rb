class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer "kind_of_booking", default: 0
      #t.user_references
      t.integer "payment_status", default: 0
      t.integer "realization_status", default: 0
      t.decimal :price_with_ducat, :precision => 10, :scale => 2
      t.decimal :heart_ducat_price, :precision => 10, :scale => 2
      t.decimal :price, :precision => 10, :scale => 2
      t.decimal :total_save, :precision => 10, :scale => 2
      #t.ngo references

      t.timestamps
    end
  end
end
