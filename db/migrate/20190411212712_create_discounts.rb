class CreateDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :discounts do |t|
      t.boolean :percent, default: false
      t.integer :price, default: 0
      t.integer :kind, default: 0
      t.integer :discount_for, default: nil
      t.integer :amount_of_discount, default: nil #only for affilate_discount
      t.references :company #only for affilate_discount
      #t.references :service      

      t.timestamps
    end
  end
end
