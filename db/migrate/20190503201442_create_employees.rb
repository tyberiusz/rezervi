class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :first_name
      t.string :last_name
      t.string :job_occupation
      t.string :description
      t.boolean :owner, default: false
      t.boolean :receive_reservation, default: true
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
