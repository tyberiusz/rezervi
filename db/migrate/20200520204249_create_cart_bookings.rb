class CreateCartBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_bookings do |t|
      t.references :cart, foreign_key: true
      t.references :company, foreign_key: true
      t.date :date_of
      t.string :hash_of_uniq, uniq: true

      t.timestamps
    end
  end
end
