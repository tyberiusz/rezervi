class AddCityRefToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_reference :companies, :city, foreign_key: true
  end
end
