# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_24_201631) do

  create_table "addresses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "street_name"
    t.string "street_number"
    t.string "flat_number"
    t.string "zip_code"
    t.string "city_name"
    t.bigint "city_id"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_addresses_on_city_id"
  end

  create_table "business_cart_items", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "business_cart_id"
    t.bigint "plan_id"
    t.integer "quantity", default: 1
    t.string "period"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "subscription_id"
    t.index ["business_cart_id"], name: "index_business_cart_items_on_business_cart_id"
    t.index ["plan_id"], name: "index_business_cart_items_on_plan_id"
    t.index ["subscription_id"], name: "index_business_cart_items_on_subscription_id"
  end

  create_table "business_carts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cart_bookings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "cart_id"
    t.bigint "company_id"
    t.date "date_of"
    t.string "hash_of_uniq"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cart_id"], name: "index_cart_bookings_on_cart_id"
    t.index ["company_id"], name: "index_cart_bookings_on_company_id"
  end

  create_table "cart_items", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "cart_id"
    t.bigint "service_id"
    t.bigint "employee_id"
    t.integer "kind_of_booking", default: 0, null: false
    t.integer "current_step", default: 0, null: false
    t.boolean "configured", default: false, null: false
    t.datetime "starts_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "cart_booking_id"
    t.index ["cart_booking_id"], name: "index_cart_items_on_cart_booking_id"
    t.index ["cart_id"], name: "index_cart_items_on_cart_id"
    t.index ["employee_id"], name: "index_cart_items_on_employee_id"
    t.index ["service_id"], name: "index_cart_items_on_service_id"
  end

  create_table "carts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "kind_of_booking", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cities", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "district"
    t.integer "voivodeship"
    t.bigint "amount"
  end

  create_table "companies", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "NIP"
    t.string "postal_code"
    t.string "street"
    t.string "street_number"
    t.integer "flat_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "city_id"
    t.bigint "category_id"
    t.index ["category_id"], name: "index_companies_on_category_id"
    t.index ["city_id"], name: "index_companies_on_city_id"
  end

  create_table "discounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "percent", default: false
    t.integer "price", default: 0
    t.integer "kind", default: 0
    t.integer "discount_for"
    t.integer "amount_of_discount"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_discounts_on_company_id"
  end

  create_table "employees", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "job_occupation"
    t.string "description"
    t.boolean "owner", default: false
    t.boolean "receive_reservation", default: true
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_employees_on_company_id"
  end

  create_table "friendly_id_slugs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, length: { slug: 70, scope: 70 }
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", length: { slug: 140 }
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "leads", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.integer "status"
    t.bigint "city_id"
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_leads_on_city_id"
  end

  create_table "managers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "login", default: "", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_managers_on_confirmation_token", unique: true
    t.index ["email"], name: "index_managers_on_email", unique: true
    t.index ["login"], name: "index_managers_on_login", unique: true
    t.index ["reset_password_token"], name: "index_managers_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_managers_on_unlock_token", unique: true
  end

  create_table "ngos", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "full_name"
    t.string "email"
    t.string "krs_number"
    t.string "nip_number"
    t.string "regon_number"
    t.integer "register_year"
    t.boolean "opp"
    t.boolean "business"
    t.boolean "future_business_possible"
    t.bigint "address_id"
    t.bigint "correspondence_address_id"
    t.bigint "city_id"
    t.integer "ambassador_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["address_id"], name: "index_ngos_on_address_id"
    t.index ["city_id"], name: "index_ngos_on_city_id"
    t.index ["correspondence_address_id"], name: "index_ngos_on_correspondence_address_id"
  end

  create_table "opening_hours", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.date "day"
    t.time "start_time", default: "2000-01-01 00:00:00", null: false
    t.time "end_time", default: "2000-01-01 00:00:00", null: false
    t.integer "day_of_week", default: 0
    t.boolean "all_of_day", default: false, null: false
    t.boolean "opened", default: false, null: false
    t.boolean "standard", default: false, null: false
    t.string "openingable_type"
    t.bigint "openingable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["openingable_type", "openingable_id"], name: "index_opening_hours_on_openingable_type_and_openingable_id"
  end

  create_table "order_bookings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "company_id"
    t.date "date_of"
    t.string "hash_of_uniq"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_order_bookings_on_company_id"
    t.index ["order_id"], name: "index_order_bookings_on_order_id"
  end

  create_table "order_items", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "service_id"
    t.bigint "employee_id"
    t.integer "kind_of_booking", default: 0, null: false
    t.integer "current_step", default: 0, null: false
    t.boolean "configured", default: false, null: false
    t.datetime "starts_at"
    t.bigint "booking_order_id"
    t.datetime "start_duration"
    t.datetime "end_duration"
    t.integer "duration_in_min"
    t.decimal "price", precision: 10, scale: 2
    t.decimal "price_save", precision: 10, scale: 2
    t.decimal "price_with_discounts", precision: 10, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["booking_order_id"], name: "index_order_items_on_booking_order_id"
    t.index ["employee_id"], name: "index_order_items_on_employee_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["service_id"], name: "index_order_items_on_service_id"
  end

  create_table "orders", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "kind_of_booking", default: 0
    t.integer "payment_status", default: 0
    t.integer "realization_status", default: 0
    t.decimal "price_with_ducat", precision: 10, scale: 2
    t.decimal "heart_ducat_price", precision: 10, scale: 2
    t.decimal "price", precision: 10, scale: 2
    t.decimal "total_save", precision: 10, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "partnerships", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_partnerships_on_company_id"
  end

  create_table "plans", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "price", precision: 6, scale: 2
    t.string "name"
    t.integer "sms_limit", default: 0
    t.integer "email_limit", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seos", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "slug"
    t.string "seoable_type"
    t.bigint "seoable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["seoable_type", "seoable_id"], name: "index_seos_on_seoable_type_and_seoable_id"
  end

  create_table "services", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "kind"
    t.datetime "start_duration"
    t.datetime "end_duration"
    t.integer "duration_in_min"
    t.decimal "price", precision: 10, scale: 2
    t.bigint "company_id"
    t.bigint "services_group_id"
    t.bigint "booking_discount_id"
    t.bigint "buying_discount_id"
    t.bigint "prepayment_discount_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["booking_discount_id"], name: "index_services_on_booking_discount_id"
    t.index ["buying_discount_id"], name: "index_services_on_buying_discount_id"
    t.index ["company_id"], name: "index_services_on_company_id"
    t.index ["prepayment_discount_id"], name: "index_services_on_prepayment_discount_id"
    t.index ["services_group_id"], name: "index_services_on_services_group_id"
  end

  create_table "services_groups", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.integer "order"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_services_groups_on_company_id"
  end

  create_table "subscriptions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "time"
    t.decimal "price", precision: 6, scale: 2, default: "0.0"
    t.boolean "discount_percent"
    t.decimal "discount", precision: 6, scale: 2, default: "0.0"
    t.bigint "plan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["plan_id"], name: "index_subscriptions_on_plan_id"
  end

  add_foreign_key "addresses", "cities"
  add_foreign_key "business_cart_items", "business_carts"
  add_foreign_key "business_cart_items", "plans"
  add_foreign_key "business_cart_items", "subscriptions"
  add_foreign_key "cart_bookings", "carts"
  add_foreign_key "cart_bookings", "companies"
  add_foreign_key "cart_items", "cart_bookings"
  add_foreign_key "cart_items", "carts"
  add_foreign_key "cart_items", "employees"
  add_foreign_key "cart_items", "services"
  add_foreign_key "companies", "categories"
  add_foreign_key "companies", "cities"
  add_foreign_key "employees", "companies"
  add_foreign_key "ngos", "addresses"
  add_foreign_key "ngos", "cities"
  add_foreign_key "partnerships", "companies"
  add_foreign_key "services", "companies"
  add_foreign_key "services", "discounts", column: "booking_discount_id"
  add_foreign_key "services", "discounts", column: "buying_discount_id"
  add_foreign_key "services", "discounts", column: "prepayment_discount_id"
  add_foreign_key "services", "services_groups"
  add_foreign_key "services_groups", "companies"
end
