Rails.application.routes.draw do
  
  scope(path_names: { new: 'aplikuj' }) do
    resources :ngos,  path: 'ambasador' , only: [:index, :new, :create] do
      get 'dziekujemy', on: :collection
      get 'scrapper_city', on: :collection
      get 'raport', on: :collection
    end
  end

  get 'welcome/index'


  root to: 'welcome#index'

  #get 'seo/show'
  #get 'business_carts/index'
  #resources :cities, path: '', only: :show
  #resources :categories, path: '', only: :show
  #resources :companies, path: '', only: :show

  #devise_for :managers
  devise_for :managers, path: 'secure',  controllers: { 
  	registrations: 'managers/registrations',
  	sessions: 'managers/sessions' },
  	path_names: { sign_in: 'login',
  	sign_out: 'logout',
  	password: 'secret',
  	confirmation: 'verification',
  	unlock: 'unblock',
  	registration: 'register',
  	sign_up: 'manager' }
  	
    get 'koszyk', to: 'carts#show', as: :cart
    get 'podsumowanie', to: 'carts#resume', as: :cart_resume
    get 'platnosc', to: 'carts#payment', as: :cart_payment
    get 'potwierdzenie', to: 'carts#confirm', as: :cart_confirm
    get 'panel', to: 'carts#panel', as: :panel
    
    resources :carts, path: 'koszyk' do
      
      member do
        get 'cart'
        
      end
      collection do
        patch 'set_kind'

      end
    end

    resources :orders, path: 'zamowienia'
  
    resources :services do
      resources :cart_items, shallow: true do
      member do
          get 'configured'
        end
      end
    end
  #get 'rejestracja-firmy', to: 'company#'
  #resources :cart_bussinesses

  get 'welcome/index'
  

  scope '/biznes' do
    get 'cennik', to: 'plans#index' 
    #get 'cart', to: 'business_carts#cart', as: 'business_cart'
    resources :business_carts, path: 'koszyk' do
      collection do
        get 'cart'
      end
    end
    resources :business_cart_items
  end

  get '/:id', to: 'seo#show', as: 'seo_link'

  root 'plans#index'
  
  #scope '/biznes' do
  #  get 'cart', to: 'business_carts#index', as: 'business_cart'
  #  resources :business_cart_items, only: [:create, :update, :destroy]
  #end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
